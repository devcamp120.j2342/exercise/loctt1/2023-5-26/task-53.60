
import models.Ngay;

public class DateClass{
    public static void main(String[] args) throws Exception {
       Ngay date1 = new Ngay(2003, 8, 27);
       Ngay date2 = new Ngay(2002, 10, 23);

       System.out.println("Date 1: ");
       System.out.println(date1.toString());

       System.out.println("--------------------------------");
       System.out.println("Date 2: ");
       System.out.println(date2.toString());
}
}
